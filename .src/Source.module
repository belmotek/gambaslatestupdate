' Gambas module file

Export

Public Path As String
Public Version_Installed As String
Public Version_InSourceDir As String
Public Version_OnGitlab As String

Public GitURL As String
Public GitUpstream As String
Public GitBranch As String = "master"
Public bHasGitFolder As Boolean

Public BranchInfo As String[]
Public MasterInfo As String[]
Public WebVer As String
Public PackagedGambas As Boolean
Public PackagedGambasList As String[]

Public Sub GetInstalledGambas()

  FMain.Msg(("Looking for installed version") & "...")
  Dim sGb3 As String = System.Find("gbr3")
  Version_Installed = FMain.ShellS(sGb3 & " -V")
  FMain.LabelVer_Ins.Text = ("Installed") & ":\nV " & Version_Installed
  FMain.Msg(("Found") & ": " & sGb3 & " " & Version_Installed)

End

Public Sub CheckRepoGambas()

  Dim sRes As String, sLN As String = Split(Distro.LinuxName, "-")[0]
  Dim sNew As String[]

  FMain.Msg(("Checking for repository installed Gambas") & "...")
  Wait 0.1
  Select sLN
    Case "ubuntu", "debian"
      sRes = Trim(FMain.ShellS("apt list --installed 'gambas3*' 2>/dev/null|grep gambas|awk {'print $1'}"))
      If sRes Then
        sNew = Split(sRes, "\n")
        For c As Integer = 0 To sNew.Max
          sNew[c] = Split(sNew[c], "/")[0]
        Next
        sRes = sNew.Join("\n")
      Endif
    Case "archlinux"
      sRes = FMain.ShellS("pacman -Qsq 'gambas3*' 2>/dev/null")
    Case "fedora"
      sRes = FMain.ShellS("dnf list -C 'gambas3*' 2>/dev/null|grep @|awk '{print $1}'")
      If sRes Then
        sNew = Split(sRes, "\n")
        For c As Integer = 0 To sNew.Max
          sNew[c] = Split(sNew[c], ".")[0]
        Next
        sRes = sNew.Join("\n")
      Endif
    Case "opensuse"
      sRes = FMain.ShellS("zypper se -i 'gambas3*'|awk '{print $3}' 2>/dev/null")
      sRes = Right(sRes, -InStr(sRes, "\ngambas"))
    Case Else

  End Select

  If Not sRes Then
    ' no gambas found
    FMain.Msg(("Gambas is not installed by packager") & ".\n" & ("Disabling auto-checking") & ".\n")
    FMain.mnuchkRepo.Value = False
    Settings["Config/GambasRepoCheck"] = False
  Else
    ' gambas found
    PackagedGambas = True
    PackagedGambasList = Split(Trim(sRes), "\n")
    FMain.Msg(("Repository installed Gambas Found!") & ".\n", sRes)
    If Message.Question(PackagedGambasList.Count & " " & ("Gambas packages found installed via") & " " & Split(Distro.InstallCommand, " ")[0] & ("install") & ". \n\n" & (" This will cause conflicts so it will be removed ") & "\n" & ("just before running") & " 'make install'\n" & ("This way you will still have Gambas if the compiling fails") & ".\n" & ("Or if you wish you can remove it now") & ".\n", ("Remove now"), ("Ok")) = 1 Then
      FMain.Wizard1.Index = 1
      FMain.Stoppable(True)
      Compile.CompileStep(Distro.UninstallPackaged())
      FMain.Stoppable(False)
      FMain.Wizard1.Index = 0

    Endif
  Endif

End

Public Sub GetVersion()

  Dim bWarnBranch As Boolean = Settings["Config/AltBranchWarning", True]

  With FMain

    If Not Path Or If Not Exist(path &/ ".git/config") Then bHasGitFolder = False Else bHasGitFolder = True

    If .bHasGit And bHasGitFolder Then GitBranch = .ShellS("git rev-parse --abbrev-ref HEAD") Else GitBranch = "master"

    .tvUser.Input(" cd " & Quote(Path) & "\nclear\n")
    .tbSrcDir.Text = Path

    FMain.Msg("\n" & ("Looking for source folder branch version") & "...")
    If .bHasGit And bHasGitFolder Then
      Version_InSourceDir = .ShellS("echo -n \"$(cat VERSION) $(git rev-parse --short " & GitBranch & ") (" & GitBranch & ")\"")
    Else
      If Exist(Path &/ "main/trunk_version.h") Then
        Version_InSourceDir = .ShellS("cat VERSION")
        Version_InSourceDir &= " " & Split(File.Load(Path &/ "main/trunk_version.h"), "\"")[1]
      Endif

    Endif
    .LabelVer_Src.Text = ("Source folder") & ": \nV" & Version_InSourceDir
    FMain.Msg(("Found") & ": " & Version_InSourceDir)

    Dim sActive As String = .ShellS("git branch")
    Dim sBranches As String[] = Split(sActive, "\n")

    Dim bMasSta As Integer[] = [0, 0]
    For c As Integer = 0 To sBranches.Max
      sBranches[c] = Trim(sBranches[c])
      If Left(sBranches[c]) = "*" Then
        sBranches[c] = Right(sBranches[c], -2)
        If sBranches[c] <> "master" And sBranches[c] <> "stable" And bWarnBranch Then
          If Message.Question(("This gambas installer is designed to only handle") & "\n" & ("the 'master' or 'stable' branch of gambas") & ".\n" & ("this folders branch is set to") & " '" & sBranches[c] & "'\n" & ("Operations may function but are not well tested") & ".", ("Stop Warnings"), ("Ok")) = 1 Then Settings["Config/AltBranchWarning"] = False
        Endif
        sActive = sBranches[c]
      Endif
      If sBranches[c] = "master" Then bMasSta[0] = 1
      If sBranches[c] = "stable" Then bMasSta[1] = 1
    Next
    'If Not bMasSta[0] Then sBranches.Add("master", 0)
    'If Not bMasSta[1] Then sBranches.Add("stable", 1)

    If sBranches.Count Then
      Object.Lock(.ComboBranch)
      .ComboBranch.List = sBranches
      If sActive Then .ComboBranch.Index = sBranches.Find(sActive)
      Object.UnLock(.ComboBranch)
    Endif

    GetURLS()

    Dim sBr As String = IIf(GitBranch <> "master" And (GitBranch <> "stable"), "master", GitBranch)

    '.btnDLZip.Tag = [GitURL[0, GitURL.Len - 4] & "/-/archive/" & GitBranch & "/gambas-" & GitBranch & ".zip",
    '  IIf(GitUpstream = "Unset", "Unset", GitUpstream[0, GitUpstream.Len - 4] & "/-/archive/" & sBr & "/gambas-" & sBr & ".zip")]

    .btnGitPull.Tag = [GitURL, GitUpstream]
    .Wizard1[1].Text = ("Compile and install from folder") & " '" & Path & "' " & ("using branch") & " '" & GitBranch & "'"

    '  .taDownload.Text = "Zip Source URL:\t" & .btnDLZip.Tag[0] & "\nUpstream URL:  \t" & .btnDLZip.Tag[1]
    '  .taDownload.Insert("\n\nGit Source URL:\t" & .btnDLGit.Tag[0] & "\nUpstream URL: \t" & .btnDLGit.Tag[1] & "\n\n")

  End With

End

Public Sub GetURLS()

  Dim Conf As String = File.Load(Path &/ ".git/config")

  Dim iPos As Integer = InStr(Conf, "[remote \"origin\"]")

  If iPos Then
    iPos = InStr(Conf, "http", iPos)
    GitURL = Conf[iPos - 1, InStr(Conf, "\n", iPos) - iPos]
  Endif
  iPos = InStr(Conf, "[remote \"upstream\"]")
  If iPos Then
    iPos = InStr(Conf, "http", iPos)
    GitUpstream = Conf[iPos - 1, InStr(Conf, "\n", iPos) - iPos]
  Else
    Debug GitUpstream
    If (Not GitUpstream) And (FMain.bHasGit) Then
      If (GitBranch <> "master") And (GitBranch <> "stable") Then FMain.ShellS("git config pull.rebase false")
    Endif

  Endif

  If Not GitUpstream Then GitUpstream = "Unset"

  FMain.btnGPUpstream.Visible = (GitUpstream <> "Unset")

End

Public Sub GetOnlineVersions()

  If (Not Path) Or (Not Exist(Path &/ ".git/config")) Then
    GitURL = "https://gitlab.com/" & FMain.MenuGF.Tag
    GitUpstream = "Unset"
    'FMain.Msg(".git/config file was not found!")
    GitBranch = FMain.ComboBranch.Text
  Else
    GetURLS()
  Endif

  FMain.Msg(("Looking for versions") & "..")

  Dim sURL As String = GitURL[0, GitURL.Len - 4] & "/-/commits/" & GitBranch
  Dim sUpstreamURL As String
  sUpstreamURL = IIf(GitUpstream = "Unset", "https://gitlab.com/gambas/gambas/-/commits/master", GitUpstream[0, GitUpstream.Len - 4] & "/-/commits/master")

  FMain.Msg("\n" & ("Getting gitlab data") & " .....")
  Dim mOrs As String = IIf(GitBranch = "stable", "stable", "master")
  FMain.Msg(("Download") & " '" & mOrs & "' " & ("version number file") & ": " & "https://gitlab.com/gambas/gambas/-/raw/" & mOrs & "/VERSION")
  Wait 0.1
  WebVer = Trim(HttpClient.Download("https://gitlab.com/gambas/gambas/-/raw/" & mOrs & "/VERSION"))
  If Not WebVer Then Error.Raise("Unable to download!")

  BranchInfo = GetCommitInfo(sURL)
  FMain.LabelGLMaster.Text = GitBranch & ":\n" & BranchInfo[0]

  FMain.Msg("\n" & ("Commit Version") & ": " & BranchInfo[0])
  FMain.Msg("\n" & ("Time: ") & BranchInfo[1], False)
  FMain.Msg(("  " & ("Author") & ": ") & BranchInfo[3])
  FMain.Msg("\n" & ("Commit message") & "...\n" & BranchInfo[2] & "\n")

  If (GitBranch <> "master") And (GitBranch <> "stable") Then
    If (InStr(sURL, "/gambas/gambas") = 0) Then
      MasterInfo = GetCommitInfo(sUpstreamURL)
      FMain.LabelGLMaster.Text = "master : " & MasterInfo[0]
      FMain.Msg("\n" & ("Commit Version") & ": " & MasterInfo[0])
      FMain.Msg("\n" & ("Time") & ": " & MasterInfo[1], False)
      FMain.Msg("  " & ("Author") & ": " & MasterInfo[3])
      FMain.Msg("\n" & ("Commit message") & "...\n" & MasterInfo[2])
    Endif
  Endif

  FMain.Msg("\n" & ("Done") & ".\n")
  FMain.taLatest.EnsureVisible()
Catch
  FMain.Msg("\n" & ("Error") & "...\n" & Error.Text)

End

Public Sub GetCommitInfo(sURL As String) As String[]

  Dim sTime, sMessage, sShort, sBr, sAuthor As String
  Dim iPos As Integer
  Dim hc As New HttpClient

  FMain.Msg("\n##########################\n\nDownloading: " & sURL, False)
  Wait 0.1

  Dim sWText As String = hc.Download(sURL)

  FMain.Msg(".......")
  sBr = File.Name(sURL)

  If Not sWText Then
    Message(("download failed"))
    If FMain.bHasGit Then sShort = FMain.ShellS("git rev-parse --short origin/" & sBr)
    Return [WebVer & " " & sShort & " (" & sBr & ")", "", "", ""]
  Endif

  iPos = InStr(sWText, "ul class=\"content-list commit-list")

  'iPos = InStr(sWText, "&middot")

  If iPos Then

    iPos = InStr(sWText, " title=", iPos) + 7
    sAuthor = sWText[iPos, InStr(sWText, "\"", iPos + 1) - iPos - 1]

    iPos = InStr(sWText, "-/commit/", iPos) + 8
    sShort = sWText[iPos, 9]

    iPos = InStr(sWText, " title=", iPos) + 7
    sTime = sWText[iPos, InStr(sWText, "\"", iPos + 1) - iPos - 1]

    iPos = InStr(sWText, ">&#x000A;", iPos)
    sMessage = Trim(Replace(sWText[iPos, InStr(sWText, "</pre", iPos + 1) - iPos - 1], "&#x000A;", "\n"))

  Endif

  Return [WebVer & " " & sShort & " (" & sBr & ")", sTime, sMessage, sAuthor]

End
