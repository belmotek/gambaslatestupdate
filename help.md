<h1 align=center><u>Gambas Compiler/Installer</u></h1>

Version "1.1.6"

### Sorry just brief help for now...

#### This utility is for upgrading your gambas basic to the latest version.

<h4>Requirements:</h4>
\* Any older version of Gambas3 above 3.8.\
\* git for git related functions.\
\* Internet connection.\
\* bash shell or another that supports <b>set -o pipefail</b> instruction.

<hr width=70%>
#### Pre-Install steps.

Ensure your repository and system is up to date.\

\* debian/ubuntu/mint\
   # sudo apt-get update\
   # sudo apt-get upgrade\


\* fedora\
   # sudo dnf update\
   # sudo dnf upgrade\


\* archLinux / manjaro\
   # sudo pacman -Syu\


\* OpenSuse\
   # sudo zypper update\


## Run the Application.

#### What the program does.. (so you do not have to)

\* Your Linux system type should be auto-detected for matching to gitlab-ci file.\

\* Note the tooltips for understanding of functions...\

\* You can check for missing needed packages/dependencies and install.\

\* Select a previously downloaded Gambas3 gitlab clone folder  and \'pull\'  updates or\
  \'git clone\' a new one with a button click.\

\* You can "Check Gitlab" to probe the gitlab gambas commits page for the latest commit info and display trunk version.\

\* Versions displayed are of gambas installed in /usr/bin , the version in the source dir and version on gitlab.\

\* If you have a custom branch with "upstream master" set you can git pull or "git pull upstream master"\

\* After getting source goto Compiler/Install page and start the compilation/install process.\



<hr width=70%>
